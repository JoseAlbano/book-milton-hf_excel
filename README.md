# My Learning Path: Data Analysis to Data Science

## Book: *Head First Excel* by Michael Milton

Hello.

Here are my Solutions to the Exercises in the Book.

They are spreadsheet files in the following formats:

- *Microsoft Excel*,
- *LibreOffice Calc* (OpenDocument), and
- *Google Sheets* ([Link](https://drive.google.com/drive/folders/1Hj1IDjl2egiBFNYFRnmezOiviGkpGKMS?usp=share_link) in Google Drive).

### My Progress

- [x] Chapter 01: Introduction to formulas: *Excel's real power*
- [x] Chapter 02: Visual design: *Spreadsheets as art*
- [x] Chapter 03: References: *Point in the right direction*
- [x] Chapter 04: Change your point of view: *Sort, zoom, and filter*
- [x] Chapter 05: Data types: *Make Excel value your values*
- [ ] Chapter 06: Dates and times: *Stay on time*
- [ ] Chapter 07: Finding functions: *Mine Excel's features on your own*
- [ ] Chapter 08: Formula auditing: *Visualize your formulas*
- [ ] Chapter 09: Charts: *Graph your data*
- [ ] Chapter 10: What if analysis: *Alternate realities*
- [ ] Chapter 11: Text functions: *Letters as data*
- [ ] Chapter 12: Pivot tables: *Hardcore grouping*
- [ ] Chapter 13: Booleans: *TRUE and FALSE*
- [ ] Chapter 14: Segmentation: *Slice and dice*
